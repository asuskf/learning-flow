Descripcion: 

Una clase de archivo que se pueda indexar y cargar por partes usando las tecnicas de peeking de Python, pero a su vez que tenga un funcionamiento parecido a las listas de Python, del punto de vista de indexación y slicing, ademas de tener funciones para retornar los headers, y clases para trabajar con los Paths de Window y Linux sin problema. 

Requerimientos: 
- [ ] Clase archivo que sea capaz de abrir los archivos y dejar el cursor abierto.
- [ ] Indexación igual a las listas de Python.
- [ ] Poder devolver la información a Pandas en IOStrings.
- [ ] Usar las clases de Path para poder tomar todos los Paths de archivos y pasarlos al formato correcto.